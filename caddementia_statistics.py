#!/usr/bin/env python
# CADDementia Challenge - web site: http://caddementia.bigr.nl - email: caddementia@bigr.nl
# January 2014

from __future__ import division

import os
import numpy
import sklearn
import sklearn.metrics
import process_results
import string
import StringIO
from scipy.stats import sem
import bootstrap
import shutil

import matplotlib
matplotlib.use('Agg') #'QT4Agg'

import matplotlib.pyplot

numpy.seterr(invalid='ignore')

def read_reference_standard(reference_file,classes):
	path=os.path.dirname(reference_file)
	f=os.path.basename(reference_file)
	ref_ids, ref_output, ref_prob_output, use_prob = process_results.read_result_file(path, f,classes)
	return ref_ids, ref_output

def match_reference(ref_ids, ref_output, ids, output, prob_output, train_or_test, results_dir, team_name, center):
	ref_ids=map(string.lower,ref_ids)
	ids=map(string.lower,ids)
	 
# 	#Select only train or test samples
# 	ref_ids_temp=[]
# 	ref_output_temp=[]
# 	for i,ref_id in enumerate(ref_ids):
# 		if train_or_test in ref_id:
# 			ref_ids_temp.append(ref_id)
# 			ref_output_temp.append(ref_output[i])
# 	ref_ids=ref_ids_temp
# 	ref_output=ref_output_temp
	
	new_ids, new_output = [],[]
	new_prob_output=map(lambda x: [], range(len(prob_output)))
	sorted_prob_output=map(lambda x: [], range(len(prob_output)))
	
	# Test for all ids if those are present in the reference
	for i,id in enumerate(ids):
		if id in ref_ids:
			new_ids.append(id)
			new_output.append(output[i])
			for j in range(0, len(prob_output)):
				new_prob_output[j].append(prob_output[j][i])
		
	# Test if all reference ids are present in the file, otherwise add		
	missing_ids=[]
	for i, ref_id in enumerate(ref_ids):
		if not (ref_id in new_ids):
			missing_ids.append(ref_id)
			new_ids.append(ref_id)
			new_output.append(len(prob_output)-1)
			for j in range(0, len(prob_output)):
				if j==len(prob_output)-1:
					new_prob_output[j].append(1)
				else:
					new_prob_output[j].append(0)
	
	# If ID are missing, write file with those ids
	if len(missing_ids) and not center:
		output=StringIO.StringIO()
		missing_file= os.path.join(results_dir, team_name + '_' + train_or_test + '_MISSING.txt')
		f=open(missing_file,"w")
		
		for id in missing_ids:
			print >>output, id
			print 'Warning: ' + id + ' is missing!'
		content=output.getvalue()
		f.write(content)
		f.close()
	
	# Sort the data					
	ref_indices=numpy.argsort(ref_ids)
	sorted_ref_ids=numpy.take(ref_ids,ref_indices)
	sorted_ref_output=numpy.take(ref_output, ref_indices)	
	
	indices=numpy.argsort(new_ids)
	sorted_ids=numpy.take(new_ids,indices)	
	sorted_output=numpy.take(new_output,indices)	
	for j in range(0, len(prob_output)):
		sorted_prob_output[j]=numpy.take(new_prob_output[j],indices)

	# Check if all output probabilities sum up to one, normalize to one if necessary
	for i,sumi in enumerate(sum(sorted_prob_output)):
		if (not (sumi>0.999 and sumi<1.001)) and (not sumi==0):
			print 'Error: The output probabilities for ' + sorted_ids[i] + ' do not sum up to 1. Please normalize.'
			exit()
			
	# Check if probabilistic outputs and diagnostic labels match.
	sorted_prob_output_transpose=map(list, zip(*sorted_prob_output))
	print sorted_output
	for i,prob in enumerate(sorted_prob_output_transpose):
		if not sum(prob)==0:
			if not ( prob.index(max(prob)) == sorted_output[i]):
				print sorted_output[i]
				print 'Error: The diagnostic label and the output probabilities do not match for ' + sorted_ids[i] 
				exit()
	
	# Check if correct data is found
	if not len(sorted_ref_ids):
		print 'ERROR: No matching subject id found. Please make sure you specified the correct input file and correct reference file' 
		quit()
		
	return sorted_ref_ids, sorted_ref_output, sorted_ids, sorted_output, sorted_prob_output

def make_confusion_matrix(classes, ref_output, output, *vars_for_save):
	# only save to file if the last 4 arguments are given:	
	if len(vars_for_save) == 4:
		team_name = vars_for_save[0]
		train_or_test = vars_for_save[1]
		centername = vars_for_save[2]
		results_dir = vars_for_save[3]
		dosave=True;
	else:
		dosave=False;
	
	# Calculate confusion matrix
	conf = sklearn.metrics.confusion_matrix (ref_output, output,range(0,len(classes)))	
	conf = numpy.transpose(conf) # Transposed to match confusion matrix on web site

	if dosave:
		file1= os.path.join(results_dir, team_name + '_' + train_or_test + '_' + centername + '_confusion_matrix.txt')		
		
		# Write table header
		outputfile=StringIO.StringIO()	
		cl = classes[:-1]
		table_header="\t".join(['', '','','True\n', ' '] + cl + ['\n'])
		f=open(file1,"w")
		outputfile.write(table_header)
		content=outputfile.getvalue()
		f.write(content)
		f.close()
		outputfile.close()    	

		# Write confusion matrix to file
		for i,row in enumerate(conf):
			outputfile=StringIO.StringIO()
			if not i:
				first = 'Pred.'	
			else:
				first = ' '
			table_header="\t".join([first, classes[i], ''])
			f=open(file1,"a")
			outputfile.write(table_header)
			content=outputfile.getvalue()
			f.write(content)
			outputfile.close()
			numpy.savetxt(f, numpy.atleast_2d(row), fmt='%4d',delimiter="\t")	
			f.close()
			
		if centername=='all':
			file2= os.path.join(results_dir, train_or_test + '_table1.txt')
			
			# Write table header
			outputfile=StringIO.StringIO()	
			cl = classes[:-1]
			table_header="\t".join([team_name, '', '','','True\n', '', ' '] + cl + ['\n'])
			f=open(file2,"a")
			outputfile.write(table_header)
			content=outputfile.getvalue()
			f.write(content)
			f.close()
			outputfile.close()    	
	
			# Write confusion matrix to file
			for i,row in enumerate(conf):
				outputfile=StringIO.StringIO()
				if not i:
					first = 'Pred.'	
				else:
					first = ' '
				table_header="\t".join(['', first, classes[i], ''])
				f=open(file2,"a")
				outputfile.write(table_header)
				content=outputfile.getvalue()
				f.write(content)
				outputfile.close()
				numpy.savetxt(f, numpy.atleast_2d(row), fmt='%4d',delimiter="\t")	
				f.close()
		
		# Make file for McNemar test: correct=1, incorrect=0
		test_correct= ref_output==output
		test_correct=test_correct.astype(int)
		file3= os.path.join(results_dir, team_name + '_' + train_or_test + '_' + centername + '_correct.txt')		
		f=open(file3,"w")
		outputfile=StringIO.StringIO()
		for k in test_correct:
			outputfile.write("%i\n" % k)
		content=outputfile.getvalue()
		test_correct=[]
		f.write(content)
		f.close()
			
    	
	return conf

def make_roc_curve(classes, ref_output, prob_output, array, team_name, train_or_test, centername, results_dir, make_figure):
	if any(t==0 for t in sum(prob_output)):
		print 'Warning: No ROC curve is calculated, output probabilities are missing'
		return
	
# 	make_figure=0
	
	# Make figure
	if make_figure:
		rocfig = matplotlib.pyplot.figure(facecolor='white')
		fs=14
		font = {'size'   : fs}
		matplotlib.rc('font', **font)
	
	# Rearrange data
	ref_output=list(ref_output)
	
	ref_missing=[]
	remove_i=[]
	for i,missing_i in enumerate(prob_output[-1]):
		if missing_i: # when data was missing
			remove_i.append(i)
			ref_missing.append(ref_output[i])
	
	prob_output_del=map(lambda x: [], range(len(prob_output)))
	if remove_i:
		ref_output_del=numpy.delete(ref_output,remove_i)
		for j,c in enumerate(classes):
			prob_output_del[j]=numpy.delete(prob_output[j],remove_i)
	else:
		ref_output_del=ref_output
		prob_output_del=prob_output		
	
	# Make ROC curve and take in account missing data
	auc_list=[];		
	for i,c in enumerate(classes):
		if c=='missing':
			continue
		y_true = [1 if y == i else 0 for y in ref_output_del]
		if not sum(y_true):
# 			print 'Warning: no ROC-curve generated for ' + c + ' ' + team_name + ' ' + train_or_test			
			continue
			
		y_score = prob_output_del[i]
		
		nr_false=y_true.count(0)
		nr_true=y_true.count(1)
		
		missed_true=ref_missing.count(i)
		missed_false=(len(ref_missing)-ref_missing.count(i))

		total_true=ref_output.count(i)
		total_false=(len(ref_output)-ref_output.count(i))

		max_sens=1-(missed_true/total_true)	
		max_spec=1-(missed_false/total_false)	

		fpr_c, tpr_c, thresholds_c = sklearn.metrics.roc_curve(y_true, y_score, pos_label=1)
		
		real_true=nr_true+missed_true
		true_factor=nr_true/real_true
		new_tpr_c=tpr_c*true_factor
		
		real_false=nr_false+missed_false
		false_factor=nr_false/real_false
		spec=1-fpr_c
		new_spec=spec*false_factor
		new_fpr_c=1-new_spec

		new_tpr_c_1=numpy.append(new_tpr_c,numpy.ones(missed_true)*new_tpr_c[-1])
		new_fpr_c_1=numpy.append(new_fpr_c,numpy.ones(missed_true))

		new_tpr_c_2=numpy.insert(new_tpr_c_1, 0, numpy.zeros(missed_false))
		new_fpr_c_2=numpy.insert(new_fpr_c_1, 0, numpy.ones(missed_false)*new_fpr_c[0])
		
		if i==1:
			for z, fpr in enumerate(new_fpr_c_2):
				tpr=new_tpr_c_2[z]
#				print str(fpr) + ' ' + str(tpr)		
	
		# Calculate area under the curve		
		auc = sklearn.metrics.auc(new_fpr_c_2, new_tpr_c_2)
		auc_list.append(auc)
#		print 'AUC for ' + c + ' : ' + str(auc)
#		print auc/true_factor/false_factor
		if make_figure:
			x=matplotlib.pyplot.plot(new_fpr_c_2,new_tpr_c_2,label=c+ ' (AUC = ' + "%.1f" % (auc * 100) + '%)')
			
			plotpoints=0
			if plotpoints:
				color=x[0].get_color()
				
				# Make 1 versus rest confusion matrix and calculate sens and spec
				notc=range(0,len(classes)-1)
				notc.remove(i)		
				
				element11=array[i][i]			
				element12=array[i][notc].sum()
				element21=numpy.transpose(array)[i][notc].sum()
				element22=sum([array[k][notc].sum() for k in notc ])			
				newarray=numpy.array([[element11, element12],[element21, element22]])
				
				sens=newarray[0][0]/numpy.transpose(newarray)[0].sum()
				spec=newarray[1][1]/numpy.transpose(newarray)[1].sum()
				h=matplotlib.pyplot.plot(1-spec,sens,marker='o',color=color)
		
		
	if make_figure:
		# Make and save plot
		matplotlib.pyplot.legend(loc=4,fontsize=fs)
		matplotlib.pyplot.xlabel('1-Specificity',fontsize=fs)
		matplotlib.pyplot.ylabel('Sensitivity',fontsize=fs)
#		matplotlib.pyplot.title('ROC curves - Team:' + team_name + ' - Data: ' + centername + ' ' + train_or_test,fontsize=fs)
		matplotlib.pyplot.grid(True)
#		matplotlib.pyplot.annotate('AD', xy=(.115, .675), xytext=(.03, .74),
#            arrowprops=dict(facecolor='black', shrink=0.01))
#		matplotlib.pyplot.annotate('MCI', xy=(.405, .585), xytext=(.45, .5),
#            arrowprops=dict(facecolor='black', shrink=0.01))
#		matplotlib.pyplot.annotate('CN', xy=(.185, .695), xytext=(.23, .61),
#            arrowprops=dict(facecolor='black', shrink=0.01))
		
#		matplotlib.pyplot.show()
	#	rocfig.savefig(os.path.join(results_dir, team_name + '_' + train_or_test + '_' + centername + '_roc.pdf'))
		rocfig.savefig(os.path.join(results_dir, team_name + '_' + train_or_test + '_' + centername + '_roc.eps'))
		fig = matplotlib.pyplot.gcf()
		fig.set_size_inches( fig.get_size_inches() * 0.6)
		ax = matplotlib.pyplot.gca()
		for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] + ax.get_xticklabels() + ax.get_yticklabels()):
			item.set_fontsize(fs*.6)
		matplotlib.pyplot.legend(loc=4,fontsize=fs*0.6)
		rocfig.savefig(os.path.join(results_dir, team_name + '_' + train_or_test + '_' + centername + '_roc.svg'))
		return auc, auc_list
	else:
		return auc, auc_list

def calculate_pairwise_auc(ref_output, prob_output, i,j):
	temp_ref_output=[]
	temp_prob_output=[]
	sum_rank_i=0
	sum_rank_j=0
	
	#select only class i and j samples
	for k_index,k in enumerate(ref_output):
		if k==i or k==j:
			temp_ref_output.append(k)
			temp_prob_output.append(prob_output[i][k_index])
		
	#rank the scores
	indices=numpy.argsort(temp_prob_output)	
	sorted_temp_ref_output=numpy.take(temp_ref_output,indices)
	
	#sum the ranks for class i
	# NB: lowest rank is assumed to be 1 in the formula
	# so we add 1 to the index.
	for index,element in enumerate(sorted_temp_ref_output):
		rank=index+1
		if element==i:
			sum_rank_i += rank

	sum_rank_i=float(sum_rank_i)
	
	# count the number of i and j samples
	ni=float(temp_ref_output.count(i))
	nj=float(temp_ref_output.count(j))
	
	#calculate the AUC for the pair
	if ni and nj:
		Aij=(sum_rank_i-(ni * (ni+1))/2)/(ni*nj)
	else:
		Aij = 0
		
	# this should never happen	
	if Aij<0 :
		Aij=0
		
	return Aij

def calculate_multiclass_auc(classes, ref_output, prob_output):
	if any(t==0 for t in sum(prob_output)):
		print 'Warning: No AUC is calculated, output probabilities are missing'
		return 0
	
	# Calculate multiclass AUC using the approach of Hand and Till (2001)
	sumA=0
	for i,ci in enumerate(classes):
		for j,cj in enumerate(classes):
			if i>=j:
				continue
#			print ci + ' ' + cj
			# calculate A(i|j)			
			Aij=calculate_pairwise_auc(ref_output, prob_output, i,j)
			
			# calculate A(j|i)
			Aji=calculate_pairwise_auc(ref_output, prob_output, j,i)
			# calculate A(i,j)
			A=(Aij+Aji)/2
#			print A
			# sum over all A(i,j)
			sumA += A
			
	# calculate M -> the multiclass AUC
	c=len(classes)-1		
	M=2/(c*(c-1))*sumA	
	
	return M

def make_table_2_and_3(train_or_test, team, centername, acc, ci_acc, tpf0, ci_tpf0, tpf1, ci_tpf1, tpf2, ci_tpf2, auc, ci_auc, results_dir):

	output=StringIO.StringIO()
	
	if train_or_test=='train':
		output_file=os.path.join(results_dir,  team + '_table2.txt')
	else:
		output_file=os.path.join(results_dir,  team + '_table3.txt')
	
	table_header="\t".join(['Data','Accuracy','CI Accuracy lower','CI Accuracy upper','TPF(CN)','CI TPF(CN) lower','CI TPF(CN) upper','TPF(MCI)','CI TPF(MCI) lower','CI TPF(MCI) upper','TPF(AD)','CI TPF(AD) lower','CI TPF(AD) upper','AUC','CI AUC lower','CI AUC upper', 'AUC (CN)','CI AUC (CN) lower','CI AUC (CN) upper', 'AUC (MCI)','CI AUC (MCI) lower','CI AUC (MCI) upper','AUC (AD)','CI AUC (AD) lower','CI AUC (AD) upper\n'])
	if centername=="all":
		f=open(output_file,"w")
		output.write(table_header)
	else:
		f=open(output_file,"a")
	
	table_template= string.Template('$centername\t$acc\t$ci_acc_l\t$ci_acc_u\t$tpf0\t$ci_tpf0_l\t$ci_tfp0_u\t$tpf1\t$ci_tpf1_l\t$ci_tpf1_u\t$tpf2\t$ci_tpf2_l\t$ci_tpf2_u\t$auc\t$ci_auc_l\t$ci_auc_u\t$auc0\t$ci_auc0_l\t$ci_auc0_u\t$auc1\t$ci_auc1_l\t$ci_auc1_u\t$auc2\t$ci_auc2_l\t$ci_auc2_u')
	new_line=table_template.safe_substitute(centername=centername, acc='%.3f'%acc, ci_acc_l='%.3f'%ci_acc[0], ci_acc_u='%.3f'%ci_acc[1], tpf0='%.3f'%tpf0, ci_tpf0_l='%.3f'%ci_tpf0[0], ci_tfp0_u='%.3f'%ci_tpf0[1], tpf1='%.3f'%tpf1, ci_tpf1_l='%.3f'%ci_tpf1[0], ci_tpf1_u=' %.3f'%ci_tpf1[1], tpf2='%.3f'%tpf2, ci_tpf2_l='%.3f'%ci_tpf2[0], ci_tpf2_u=' %.3f'%ci_tpf2[1], auc='%.3f'%auc[0], ci_auc_l='%.3f'%ci_auc[0][0], ci_auc_u=' %.3f'%ci_auc[0][1], auc0='%.3f'%auc[1], ci_auc0_l='%.3f'%ci_auc[1][0], ci_auc0_u=' %.3f'%ci_auc[1][1], auc1='%.3f'%auc[2], ci_auc1_l='%.3f'%ci_auc[2][0], ci_auc1_u=' %.3f'%ci_auc[2][1], auc2='%.3f'%auc[3], ci_auc2_l='%.3f'%ci_auc[3][0], ci_auc2_u=' %.3f'%ci_auc[3][1])
	print >>output, new_line 
	content=output.getvalue()
#	print table_header
#	print content
	f.write(content)
	
	f.close()
	output.close()
	
def make_table_3_all(train_or_test, team, centername, acc, ci_acc, tpf0, ci_tpf0, tpf1, ci_tpf1, tpf2, ci_tpf2, auc, ci_auc, results_dir):
	output=StringIO.StringIO()
	
	output_file=os.path.join(results_dir,  train_or_test + '_table3.txt')
	
	table_header="\t".join(['Algorithm','Data','Accuracy','CI Accuracy lower','CI Accuracy upper','TPF(CN)','CI TPF(CN) lower','CI TPF(CN) upper','TPF(MCI)','CI TPF(MCI) lower','CI TPF(MCI) upper','TPF(AD)','CI TPF(AD) lower','CI TPF(AD) upper','AUC','CI AUC lower','CI AUC upper\n'])
	if not os.path.exists(output_file):
		f=open(output_file,"w")
		output.write(table_header)
	else:
		f=open(output_file,"a")
	
	table_template= string.Template('$team\t$centername\t$acc\t$ci_acc_l\t$ci_acc_u\t$tpf0\t$ci_tpf0_l\t$ci_tfp0_u\t$tpf1\t$ci_tpf1_l\t$ci_tpf1_u\t$tpf2\t$ci_tpf2_l\t$ci_tpf2_u\t$auc\t$ci_auc_l\t$ci_auc_u')
	new_line=table_template.safe_substitute(team=team,centername=centername, acc='%.3f'%acc, ci_acc_l='%.3f'%ci_acc[0], ci_acc_u='%.3f'%ci_acc[1], tpf0='%.3f'%tpf0, ci_tpf0_l='%.3f'%ci_tpf0[0], ci_tfp0_u='%.3f'%ci_tpf0[1], tpf1='%.3f'%tpf1, ci_tpf1_l='%.3f'%ci_tpf1[0], ci_tpf1_u=' %.3f'%ci_tpf1[1], tpf2='%.3f'%tpf2, ci_tpf2_l='%.3f'%ci_tpf2[0], ci_tpf2_u=' %.3f'%ci_tpf2[1], auc='%.3f'%auc[0], ci_auc_l='%.3f'%ci_auc[0][0], ci_auc_u=' %.3f'%ci_auc[0][1])
	print >>output, new_line 
	content=output.getvalue()
#	print table_header
#	print content
	f.write(content)
	
	f.close()
	output.close()	

def make_table_4(train_or_test, team, acc, ci_acc, tpf0, ci_tpf0, tpf1, ci_tpf1, tpf2, ci_tpf2, auc, ci_auc, rank_acc, rank_auc, results_dir):
	output=StringIO.StringIO()
	
	output_file=os.path.join(results_dir,  train_or_test + '_table4.txt')
	
	table_header="\t".join(['Algorithm','Accuracy','CI Accuracy lower','CI Accuracy upper','TPF(CN)','CI TPF(CN) lower','CI TPF(CN) upper','TPF(MCI)','CI TPF(MCI) lower','CI TPF(MCI) upper','TPF(AD)','CI TPF(AD) lower','CI TPF(AD) upper','AUC','CI AUC lower','CI AUC upper','Rank based on accuracy','Rank based on AUC\n'])
	if not os.path.exists(output_file):
		f=open(output_file,"w")
		output.write(table_header)
	else:
		f=open(output_file,"a")
	
	table_template= string.Template('$team\t$acc\t$ci_acc_l\t$ci_acc_u\t$tpf0\t$ci_tpf0_l\t$ci_tfp0_u\t$tpf1\t$ci_tpf1_l\t$ci_tpf1_u\t$tpf2\t$ci_tpf2_l\t$ci_tpf2_u\t$auc\t$ci_auc_l\t$ci_auc_u\t$rank_acc\t$rank_auc')
	new_line=table_template.safe_substitute(team=team, acc='%.3f'%acc, ci_acc_l='%.3f'%ci_acc[0], ci_acc_u='%.3f'%ci_acc[1], tpf0='%.3f'%tpf0, ci_tpf0_l='%.3f'%ci_tpf0[0], ci_tfp0_u='%.3f'%ci_tpf0[1], tpf1='%.3f'%tpf1, ci_tpf1_l='%.3f'%ci_tpf1[0], ci_tpf1_u=' %.3f'%ci_tpf1[1], tpf2='%.3f'%tpf2, ci_tpf2_l='%.3f'%ci_tpf2[0], ci_tpf2_u=' %.3f'%ci_tpf2[1], auc='%.3f'%auc[0], ci_auc_l='%.3f'%ci_auc[0][0], ci_auc_u=' %.3f'%ci_auc[0][1], rank_acc=rank_acc, rank_auc=rank_auc)
	print >>output, new_line 
	content=output.getvalue()
#	print table_header
#	print content
	f.write(content)
	
	f.close()
	output.close()
	
def make_table_4extra(train_or_test, team, acc, ci_acc, tpf0, ci_tpf0, tpf1, ci_tpf1, tpf2, ci_tpf2, auc, ci_auc, rank_acc, rank_auc, results_dir):
	output=StringIO.StringIO()
	
	output_file=os.path.join(results_dir,  train_or_test + '_table4extra.txt')
	
	table_header="\t".join(['Algorithm','Accuracy','CI Accuracy lower','CI Accuracy upper','TPF(CN)','CI TPF(CN) lower','CI TPF(CN) upper','TPF(MCI)','CI TPF(MCI) lower','CI TPF(MCI) upper','TPF(AD)','CI TPF(AD) lower','CI TPF(AD) upper','AUC','CI AUC lower','CI AUC upper','Rank based on accuracy','Rank based on AUC', 'AUC (CN)','CI AUC (CN) lower','CI AUC (CN) upper', 'AUC (MCI)','CI AUC (MCI) lower','CI AUC (MCI) upper','AUC (AD)','CI AUC (AD) lower','CI AUC (AD) upper\n'])
	if not os.path.exists(output_file):
		f=open(output_file,"w")
		output.write(table_header)
	else:
		f=open(output_file,"a")
	
	table_template= string.Template('$team\t$acc\t$ci_acc_l\t$ci_acc_u\t$tpf0\t$ci_tpf0_l\t$ci_tfp0_u\t$tpf1\t$ci_tpf1_l\t$ci_tpf1_u\t$tpf2\t$ci_tpf2_l\t$ci_tpf2_u\t$auc\t$ci_auc_l\t$ci_auc_u\t$rank_acc\t$rank_auc\t$auc0\t$ci_auc0_l\t$ci_auc0_u\t$auc1\t$ci_auc1_l\t$ci_auc1_u\t$auc2\t$ci_auc2_l\t$ci_auc2_u')
	new_line=table_template.safe_substitute(team=team, acc='%.3f'%acc, ci_acc_l='%.3f'%ci_acc[0], ci_acc_u='%.3f'%ci_acc[1], tpf0='%.3f'%tpf0, ci_tpf0_l='%.3f'%ci_tpf0[0], ci_tfp0_u=' %.3f'%ci_tpf0[1], tpf1='%.3f'%tpf1, ci_tpf1_l='%.3f'%ci_tpf1[0], ci_tpf1_u=' %.3f'%ci_tpf1[1], tpf2='%.3f'%tpf2, ci_tpf2_l='%.3f'%ci_tpf2[0], ci_tpf2_u=' %.3f'%ci_tpf2[1], auc='%.3f'%auc[0], ci_auc_l='%.3f'%ci_auc[0][0], ci_auc_u=' %.3f'%ci_auc[0][1], rank_acc=rank_acc, rank_auc=rank_auc, auc0='%.3f'%auc[1], ci_auc0_l='%.3f'%ci_auc[1][0], ci_auc0_u=' %.3f'%ci_auc[1][1], auc1='%.3f'%auc[2], ci_auc1_l='%.3f'%ci_auc[2][0], ci_auc1_u=' %.3f'%ci_auc[2][1], auc2='%.3f'%auc[3], ci_auc2_l='%.3f'%ci_auc[3][0], ci_auc2_u=' %.3f'%ci_auc[3][1])
	print >>output, new_line 
	content=output.getvalue()
#	print table_header
#	print content
	f.write(content)
	
	f.close()
	output.close()	
	
def check_centers(ids):

	centers=['all']
	possible_centers=['emc','vumc', 'up', 'icl']
	
	indices=[]
	indices.append(range(0,len(ids)))

	for c in possible_centers:
		if any(c in s.lower() for s in ids):
			centers.append(c)
			matching_index = [i for i,s in enumerate(ids) if c in s.lower()]
			
			indices.append(matching_index)
	
	return centers, indices

def write_to_file(list1,results_dir, name):
	output_file=os.path.join(results_dir,  name + '.txt')
	f=open(output_file,"w")
	for item in list1:
		f.write("%s\n" % item)

def bootstrap_ci(list_in, my_function,n_bootstrap):
	output=bootstrap.ci(numpy.arange(len(list_in)), statfunction=my_function, alpha=0.05, n_samples=n_bootstrap, method='bca')
	
	ci=output[0]
	bootstrap_samples=output[1]

	return ci, bootstrap_samples
	
def main(reference_file, ids_original,output_original,prob_output_original, team_name, train_or_test, results_dir, classes, n_bootstraps):
	ref_ids_original, ref_output_original = read_reference_standard(reference_file, classes)
	centers, indices=check_centers(ref_ids_original);
		 
	for center, centername in enumerate(centers):	
		# Select data from specific center
		print centername
		ref_ids = [ref_ids_original[k] for k in indices[center]]
		ref_output = [ref_output_original[k] for k in indices[center]]
		
		# Match reference data with results
		ref_ids, ref_output, ids, output, prob_output = match_reference(ref_ids, ref_output, ids_original, output_original, prob_output_original, train_or_test, results_dir, team_name, center)
		
# 		sample30=True
# 		if sample30:
# 			import random
# 			if len(ref_ids)<=27:
# 				sampleof30=range(0, len(ref_ids)-1)
# 			else:
# 				sampleof30=random.sample(range(0, len(ref_ids)-1), 27)
# 			ref_ids=ref_ids[sampleof30]
# 			ref_output=ref_output[sampleof30]
# 			ids=ids[sampleof30]
# 			output=output[sampleof30]
# 			for c in range(0,len(prob_output)):
# 					prob_output[c]=prob_output[c][sampleof30]
		
		# Make confusion matrix and save to file
		array=make_confusion_matrix(classes, ref_output, output, team_name, train_or_test, centername, results_dir)
		
		# Accuracy
		# Number of correctly classified elements (trace) divided by the total number of samples.			
		accuracy=array.trace()/array.sum()
		def function_acc(i):
			array_b=make_confusion_matrix(classes, ref_output[i], output[i])
			return array_b.trace()/array_b.sum()
		ci_acc,bootstrap_acc=bootstrap_ci(ref_output,function_acc,n_bootstraps)
		
		write_to_file(bootstrap_acc, results_dir, team_name + '_' + centername + '_bootstrap_accuracy')
		print 'Accuracy\t:\t' + '%.3f'%accuracy +' ( ci: ' + '%.3f'%ci_acc[0] + ' ' + '%.3f'%ci_acc[1] + ' )'
		
		# True positive fraction
		def function_tpf(i):
			array_b=make_confusion_matrix(classes, ref_output[i], output[i])
			if not numpy.transpose(array_b)[c].sum():
				return 0
			else:
				return array_b[c][c]/numpy.transpose(array_b)[c].sum()
		
		tpf=[]
		ci_tpf=[]
		for c in range(0,len(prob_output)-1):	
			tpf_c=function_tpf(range(0,len(ref_output)))
			ci_tpf_c, bootstrap_tpf_c=bootstrap_ci(ref_output,function_tpf,n_bootstraps)
			tpf.append(tpf_c)
			ci_tpf.append(ci_tpf_c)
						
			write_to_file(bootstrap_tpf_c, results_dir, team_name + '_' + centername + '_bootstrap_tpf_' + str(c))
			print 'TPF for ' + classes[c] + '\t:\t' + '%.3f'%tpf[c] +' ( ci: ' + '%.3f'%ci_tpf_c[0] + ' ' + '%.3f'%ci_tpf_c[1] + ' )'

		if len(prob_output)<4:
			tpf.append(0)
			ci_tpf.append([0,0])

		# Multiclass AUC
		if len(classes) > 3:	
			def function_auc(i): 
				prob_output_bootstrap=map(lambda x: [], range(len(prob_output)))
				for c in range(0,len(prob_output)):
					prob_output_bootstrap[c]=prob_output[c][i]
					
				

				dummy, auc_list = make_roc_curve(classes, ref_output[i], prob_output_bootstrap, array, team_name, train_or_test, centername, results_dir, make_curve)									
				if make_curve:
					return calculate_multiclass_auc(classes, ref_output[i], prob_output_bootstrap), auc_list	
				else:
					if which_auc==0:
						return calculate_multiclass_auc(classes, ref_output[i], prob_output_bootstrap)
					else:
						return auc_list[which_auc-1]
				
		
		else: 
			def function_auc(i):
				prob_output_bootstrap=map(lambda x: [], range(len(prob_output)))
				for c in range(0,len(prob_output)):
						prob_output_bootstrap[c]=prob_output[c][i]
				return make_roc_curve(classes, ref_output[i], prob_output_bootstrap, array, team_name, train_or_test, centername, results_dir, make_curve)
		
		# compute confidence intervals using bootstrap
		auc=0		
		auc_list=[0,0,0]
		ci_auc=[[0, 0]]*len(classes)
		bootstrap_auc=[auc] *len(classes) 
		if any(t==0 for t in sum(prob_output)):
			print 'Warning: No AUC confidence interval is calculated, output probabilities are missing'			
			src_svg=os.path.join(os.curdir,'no_roc_curve.svg')
			new_svg=os.path.join(results_dir, team_name + '_' + train_or_test + '_' + centername + '_roc.svg')
			shutil.copyfile(src_svg,new_svg)

		else:
			# Compute AUC on original set, without bootstrap, and save ROC curve
			make_curve=1
			auc, auc_list=function_auc(range(0,len(ref_output)))
			
			make_curve=0
			
			for which_auc in range(0,len(classes)):
				ci_auc[which_auc],bootstrap_auc[which_auc]=bootstrap_ci(ref_output,function_auc,n_bootstraps)				
		
		auc=[auc] + auc_list
		
		# TODO: check if the following commented code indeed can be replaced by the lines above			
		# this check below goes wrong if the first prob output happens to be 0.
		#if prob_output[0][0]:
		#	ci_auc,bootstrap_auc=bootstrap_ci(ref_output,function_auc,n_bootstraps)
		#else:
		#	ci_auc=[0, 0]
		#	bootstrap_auc=[0] * n_bootstraps

		write_to_file(bootstrap_auc, results_dir, team_name + '_' + centername + '_bootstrap_auc')
		
		type_of_aucs=['AUC\t'] + ['AUC(' + cc + ')  ' for cc in classes]
		
		for which_auc in range(0,len(classes)):
			print type_of_aucs[which_auc] + '\t:\t' + '%.3f'%auc[which_auc] +' ( ci: ' + '%.3f'%ci_auc[which_auc][0] + ' ' + '%.3f'%ci_auc[which_auc][1] + ' )'
				
		# Tables
		make_table_2_and_3(train_or_test, team_name, centername, accuracy, ci_acc, tpf[0], ci_tpf[0], tpf[1], ci_tpf[1], tpf[2], ci_tpf[2], auc, ci_auc, results_dir)
		make_table_3_all(train_or_test, team_name, centername, accuracy, ci_acc, tpf[0], ci_tpf[0], tpf[1], ci_tpf[1], tpf[2], ci_tpf[2], auc, ci_auc, results_dir)
		
		if centername=='all' and train_or_test=='test':
			make_table_4(train_or_test, team_name, accuracy, ci_acc, tpf[0], ci_tpf[0], tpf[1], ci_tpf[1], tpf[2], ci_tpf[2], auc, ci_auc,0,0,results_dir)
			make_table_4extra(train_or_test, team_name, accuracy, ci_acc, tpf[0], ci_tpf[0], tpf[1], ci_tpf[1], tpf[2], ci_tpf[2], auc, ci_auc,0,0,results_dir)
		
if __name__ == '__main__':
    main()