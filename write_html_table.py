#!/usr/bin/env python
# Ranking script for the CADDementia challenge.
#
# CADDementia Challenge - web site: http://caddementia.bigr.nl - email: caddementia@bigr.nl
# July 2014

import os
import csv
import string
import math
import sys
import time
import datetime
from optparse import OptionParser


def main(arg0):
	
	path=os.path.dirname(arg0)
	filebase=os.path.basename(arg0)
	fileprefix, ext = os.path.splitext(filebase)
	
	html = open(os.path.join(path, fileprefix + '.html'),'w')
	html.write('<table  class="comictable sortable smallfont" style="width: 999px;" >\n')
	html.write('<thead>')

	max_len=0
	if 'ranked' in arg0:
		date_in_table=True
	else:
		date_in_table=False
		
	with open( arg0, 'rb') as csvfile:
		reader = csv.reader( csvfile ,delimiter='\t')

		i=1
		acc_table=[]
		auc_table=[]
		for row in reader:
			row_acc=[]
			row_auc=[]
			ci=0
			for indexj,j in enumerate(row): 
				if row[0] in ['Data', 'all','vumc','emc','up'] and j.strip()=='1.000':
					j='0.999'
				try:
					elementj='%i'%int(j)
				except ValueError:
					try:
						elementj=float(j)
						if elementj==999.0:
							elementj='-'
						elif elementj<1:
							ci=ci+1
							elementj='%.1f'%(elementj*100)							
							if ci==1 and not (indexj==17 or indexj==28):
								elementj_temp=elementj
								elementj=''
							elif ci==2:	
								elementj_temp=elementj_temp + '<br>[' + elementj
								elementj=''
							elif ci==3 and float(j)==0:
								elementj='-'
								ci=0
							elif ci==3:
								elementj=elementj_temp + ' - ' + elementj + ']'
								ci=0	
							else:
								elementj='-'
								ci=0																						
						else:
							elementj='%.1f'%elementj
					except ValueError:
						if 'acc' in j.lower() or 'tpf' in j.lower():
							acc_table.append(indexj)
						elif 'auc' in j.lower():
							auc_table.append(indexj)
						else:
							acc_table.append(indexj)
							auc_table.append(indexj)
						
						if (' (CN)' in j):
							j=j.replace(' (CN)', '<sub>CN</sub>')
						elif (' (MCI)' in j):
							j=j.replace(' (MCI)', '<sub>MCI</sub>')
						elif (' (AD)' in j):
							j=j.replace(' (AD)', '<sub>AD</sub>')
						elif ('(CN)' in j):
							j=j.replace('(CN)', '<sub>CN</sub>')
						elif ('(MCI)' in j):
							j=j.replace('(MCI)', '<sub>MCI</sub>')
						elif ('(AD)' in j):
							j=j.replace('(AD)', '<sub>AD</sub>')
							
						if ('Rank' in j):
							elementj=j.replace(' based on ', '<br>')
							
						elif indexj==0:
							elementj=j
						elif (not 'CI ' in j):
							elementj=j + ' [CI] (%)'
						else:
							elementj=''
							
# 						if ('Rank<br>accuracy' in elementj):	
# 							elementj=elementj.replace('accuracy', 'Acc')
							
						if elementj=='AUC [CI] (%)':
							elementj='AUC<sub>all</sub> [CI] (%)'
				
				

				if elementj: #and (indexj in acc_table)		
					row_acc.append(elementj)
				if elementj and (indexj in auc_table):
					row_auc.append(elementj)
					
							
			if row[0]=='Algorithm' and date_in_table:
				row_acc.append('Date')
				row_acc.append('Doc.')
# 				row_acc.append('Wiki')
			elif date_in_table:
				name=row[0].split('-')
				outputfile=os.path.join('/home','ebron', 'CADDementia','output_files_names','results_test_' + row[0] + '.txt')
				if not os.path.exists(outputfile):
					timeoffile=''
				else:
					file_time=time.ctime(os.path.getmtime(outputfile))  
					file_time=datetime.datetime.strptime(file_time, "%a %b %d %H:%M:%S %Y")
					ref_date=datetime.datetime.strptime('01/09/2014', "%d/%m/%Y")
					if file_time.date() < ref_date.date():
						timeoffile='15/06/2014'
						link_presentation='<a href=http://caddementia.grand-challenge.org/site/CADDementia/serve/public_html/presentations/'+ name[0] +'.pdf>Pres.</a>'
					else:
						timeoffile=file_time.strftime('%d/%m/%Y')
						row_acc[0]=row_acc[0]+'*'	
						link_presentation='    '
				if name[0] == 'Bron':
						row_acc[0]=row_acc[0]+'<sup>,</sup>**'	
				row_acc.append(timeoffile)
							
				if name[0]== 'Dolph' and file_time.date() == datetime.datetime.strptime('09/09/2015', "%d/%m/%Y").date():
					name[0] = 'Dolph2015_9sep2015'
				elif name[0]== 'Dolph' and file_time.date() > ref_date.date():
					name[0] = 'Dolph2015'	
				elif name[0] == 'Cardenas':
					name[0] = 'Cardenas2017'
				

				link_site='<a href=http://caddementia.grand-challenge.org/site/CADDementia/serve/public_html/articles/'+ name[0] +'.pdf><img src=http://caddementia.grand-challenge.org/site/CADDementia/serve/public_html/pdf.jpg width=15mm></a>'
# 				row_acc.append(link_site)
				
				link_wiki='<a href=http://wiki.caddementia.bigr.nl/index.php/'+ name[0] +'_et_al. target="_blank"><img src=http://caddementia.grand-challenge.org/site/CADDementia/serve/public_html/wiki.jpg width=15mm></a>'
				
				docs=link_site + ' | ' + link_wiki 
# 				+ ' | ' + link_presentation
				row_acc.append(docs)
				
			max_len=max([max_len,len(row_acc)])
			while len(row_acc)<max_len:
				row_acc.append('-')
			#
			if row_acc[0] in ['Data', 'all','vumc','emc','up']:
				new_row_acc='<tr>' + \
				'<td>' + \
				string.join( row_acc, '</td><td>' ) + \
				'</td>' + \
				'</tr>\n'			
			else:
				new_row_acc='<tr>' + \
				'<td>' + \
				string.join( [row_acc[0]] +[row_acc[6]]+[row_acc[-4]] + row_acc[1:5] + [row_acc[7]] +[row_acc[-3]]+[row_acc[5]] + row_acc[8:-5] + row_acc[-2:], '</td><td>' ) + \
				'</td>' + \
				'</tr>\n'
			print new_row_acc
			html.write(new_row_acc)		

			if i:
				html.write('</thead>')	
				html.write('<tbody>')
				i=0
				
		html.write('</tbody>')	
		html.write('</table>\n')		
	
if __name__ == '__main__':
	# Parse input arguments
	parser = OptionParser(description="HTML table script for the CADDementia challenge.", usage="Usage: python %prog [options] filename. Use option -h for help information.")
	(options, args) = parser.parse_args()

	if len(args) != 1:
		parser.error("wrong number of arguments")
	
	main(args[0])
