#!/usr/bin/env python
# Evaluation script for the CADDementia challenge. 
# Usage: ./process_results [options] filename
# Prerequisites: Python (including os, csv, string, StringIO, OptionParser, numpy, sklearn)
#
# CADDementia Challenge - web site: http://caddementia.bigr.nl - email: caddementia@bigr.nl
# January 2014

import os
import csv
import string
import math
import re
import caddementia_statistics
import rank_algorithms
from optparse import OptionParser

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def get_result_files(file1):
	correct_files=[]
	team_names=[]
	train_or_test=[]
	
	# ensure that file1 is a list
	files=[]
	files.append(file1)
	
	for f in files:
		f_basename= os.path.splitext(f)[0]
		try:
			[a,b,c]=string.split(f_basename,'_', 2)
			if not (a.lower() == 'results' or a.lower() == 'result'):
				print f + ' is no valid result file name. Format: results_{train/test}_{teamname}.txt'
				continue
			elif not (b.lower() == 'train' or b.lower() == 'test'):
				print f + ' is no valid result file name. Format: results_{train/test}_{teamname}.txt'
				continue
			else:
				correct_files.append(f)
				team_names.append(c)
				train_or_test.append(b.lower())
				
		except ValueError:
			print f + ' is no valid result file name'
			continue		
	return correct_files, team_names, train_or_test

def read_result_file(path, f, classes):
	if not os.path.exists(os.path.join(path,f)):
		print 'ERROR: ' + os.path.join(path,f) + ' does not exist. Please specify a valid result file'
		quit()
	f_content = open(os.path.join(path,f), "r")			
	r = csv.DictReader(f_content,delimiter='\t')		
	ids, output, = [],[]
	prob_output=map(lambda x: [], range(len(classes)))

	for row in r:
#		print row
		# Check number of columns
		if not ((len(row) == len(classes) + 1) or (len(row)== 2)  or (len(row)== 3) or (len(row)== 4)):
			print 'ERROR: ' + str(len(row)) + ' column(s) is/are found in the text file, 5 columns are expected. Please check if the right separator <tab> was used.'
			exit()
			
		# Load subject ID
		try:
			print row['ID']
			id=row['ID']
			ids.append(id.strip())
		except KeyError:
			try:
				id=row['id']
				ids.append(id.strip())
			except KeyError:
				print 'KeyError: Make sure that in the text file the column with the subject name is called "ID"'
				exit()
				
		# Load diagnostic labels
		try:

			print row['output']
			output.append(row['output'].strip())
		except KeyError:
			print 'KeyError: Make sure that in the text file the column with the diagnoses is called "output"'
			exit()
					
		use_prob=1
		for i,c in enumerate(classes): 
			temp=prob_output[i]
			if c.lower()=='missing':
				p='0'
			else:
				try:
					p=row['p' + c.upper()]
				except KeyError:
					try:	
						p=row['p' + c.lower()]
					except KeyError:
						p='0'
						use_prob=0
				if not is_number(p) or math.isnan(float(p)):
					print 'Error: p' + c.upper() + ' is not a number for ' + id
					exit()
				elif float(p)<0:
					print 'Error: p' + c.upper() + ' is negative for ' + id
					exit()
					
			temp.append(float(p))
			prob_output[i]=temp
	output = map(int, output)
	return ids, output, prob_output, use_prob
	
def main():
	# Parse input arguments
	parser = OptionParser(description="Evaluation script for the CADDementia challenge.", usage="Usage: python %prog [options] filename. Use option -h for help information.")
	parser.add_option("-r", "--reference", dest="reference_file", default="reference_test.txt", help='the file with the ground truth diagnoses')
	parser.add_option("-o", "--output", dest="results_dir", default="./results", help='the output directory')
	parser.add_option("-c", "--classes", dest="classes", default='CN MCI AD', help='classes in the correct order (default: CN=0, MCI=1, AD=2, missing=3)')
	parser.add_option("-n", "--n_bootstraps", dest="n_bootstraps", default='1000', help='number of bootstrap for confidence interval estimation (default: 1000)')
	(options, args) = parser.parse_args()

	if len(args) != 1:
		parser.error("wrong number of arguments")
		
	path=os.path.dirname(args[0])
	filebase=os.path.basename(args[0])
	
#	classes=options.classes.split(' ')
	classes=re.split('_| ',options.classes)
	classes.append('missing')
	
	n_bootstraps=int(options.n_bootstraps)
	
	# check if reference file exists
	if not os.path.exists(options.reference_file):
		parser.error(options.reference_file + ' does not exist. Please specify a correct reference file (option -r).')
	
	# check if output directory exists
	if not os.path.exists(options.results_dir):
		os.mkdir(options.results_dir)
	
	# get the result files with the correct file name and extract the team_names
	correct_files, team_names, train_or_test=get_result_files(filebase)
	print correct_files
	
	for j,f in enumerate(correct_files):
		# read the result file
		ids, output, prob_output, use_prob = read_result_file(path, f,classes)
		print ids
		
		if not use_prob:
			print 'Warning: No probabilistic outcomes are defined in ' + args[0] + '. Only diagnostic labels are used for evaluation.'
		
		print '-----------------------------'
		print 'Input file\t:\t' + f
		print 'Reference file\t:\t' + options.reference_file
		print 'Output folder\t:\t' + options.results_dir
		print '-----------------------------'
		# calculate evaluation measures
		caddementia_statistics.main(options.reference_file, ids,output,prob_output, team_names[j], train_or_test[j],options.results_dir,classes,n_bootstraps)
		print '-----------------------------'
		
#	rank_algorithms.main(os.path.join(options.results_dir, 'test_table4extra.txt'))
if __name__ == '__main__':
	main()		
