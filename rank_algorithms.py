#!/usr/bin/env python
# Ranking script for the CADDementia challenge.
#
# CADDementia Challenge - web site: http://caddementia.bigr.nl - email: caddementia@bigr.nl
# July 2014

import os
import csv
import string
import math
import re
import caddementia_statistics
from optparse import OptionParser
import numpy
import scipy.stats as ss

def main():
	# Parse input arguments
	parser = OptionParser(description="Ranking script for the CADDementia challenge.", usage="Usage: python %prog [options] filename. Use option -h for help information.")
	(options, args) = parser.parse_args()

	if len(args) != 1:
		parser.error("wrong number of arguments")

	path=os.path.dirname(args[0])
	filebase=os.path.basename(args[0])

	f_content = open(os.path.join(path,filebase), "r")
	r = csv.DictReader(f_content,delimiter='\t')
	name, acc, auc = [],[],[]
	acc_with_organizers, auc_with_organizers = [],[]
	for row in r:
		print row
		name.append(row['Algorithm'].strip())
		acc_with_organizers.append(row['Accuracy'].strip())
		auc_with_organizers.append(row['AUC'].strip())
		
		if row['Algorithm'].strip()[0:4]=='Bron':
			acc.append('0')
			auc.append('0')
		else:
			acc.append(row['Accuracy'].strip())
			auc.append(row['AUC'].strip())
		
	# Get ranks	
	rank_acc_with_organizers=len(acc_with_organizers)-ss.rankdata(acc_with_organizers)+1
	rank_auc_with_organizers=len(auc_with_organizers)-ss.rankdata(auc_with_organizers)+1
	
	rank_acc=len(acc)-ss.rankdata(acc)+1
	rank_auc=len(auc)-ss.rankdata(auc)+1	
	
# 	for i,rank in enumerate(rank_acc_no_organizers):
# 		if not acc_no_organizers[i]:
# 			print i
# 			if i==len(rank_acc_no_organizers)-1:
# 				rank_acc_no_organizers[i]=rank_acc_no_organizers[i-1]		
# 			else:
# 				rank_acc_no_organizers[i]=rank_acc_no_organizers[i+1]		
# 	for i,rank in enumerate(rank_auc_no_organizers):
# 		if not auc_no_organizers[i]:
# 			if i==len(rank_acc_no_organizers)-1:
# 				rank_auc_no_organizers[i]=rank_auc_no_organizers[i-1]
# 			else:
# 				rank_acc_no_organizers[i]=rank_acc_no_organizers[i+1]	
# 	
# 	rank_acc=rank_acc_no_organizers
# 	rank_auc=rank_auc_no_organizers

	for i,x in enumerate(auc):
		if x == '0.000':
			rank_auc[i]=0	
			rank_auc_with_organizers[i]=0	
			
	for i,x in enumerate(acc):
		if x == '0':
			rank_acc[i]=999	
			rank_auc[i]=999	
	
	f_content.seek(0)
	f_new = open(os.path.join(path,'ranked_' + filebase ), "wb")
	r.fieldnames.append('Rank based on accuracy**')
	r.fieldnames.append('Rank based on AUC**')
	print r.fieldnames
	r_new = csv.DictWriter(f_new, r.fieldnames,delimiter='\t')
	r_new.writeheader()				
	
	
	# Sort the file on the accuracy ranks and write to new file
	# The indexes for the new file
	ind_list=len(acc)-ss.rankdata(acc,method='ordinal')+1
	# Loop over the indexes
	for j in range(1,len(ind_list)+1):
		ind=numpy.where(ind_list==j)[0][0]
# 		print ind
		# Go to the beginning of the first file
		f_content.seek(0)
		i=0
		# Go to good row in first file 
		for row in r:
			if i<=ind:
				i=i+1
				continue
			# Fill in the ranks in the row
			row['Rank based on accuracy']='%.1f'%rank_acc[ind]
			row['Rank based on AUC']='%.1f'%rank_auc[ind]
			row['Rank based on accuracy**']='%.1f'%rank_acc_with_organizers[ind]
			row['Rank based on AUC**']='%.1f'%rank_auc_with_organizers[ind]			
# 			print row
			# Save to file
			r_new.writerow(row)	
			break
	
#			
	

if __name__ == '__main__':
        main()
