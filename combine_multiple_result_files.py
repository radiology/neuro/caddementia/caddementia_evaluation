#!/usr/bin/env python
# CADDementia Challenge - web site: http://caddementia.bigr.nl - email: caddementia@bigr.nl
# July 2014

from __future__ import division

import os
import numpy
import sklearn
import sklearn.metrics
import process_results
import caddementia_statistics
import string
import StringIO
from scipy.stats import sem
from optparse import OptionParser
import re


def main():
	# Parse input arguments
	parser = OptionParser(description="Combine result files for the CADDementia challenge.", usage="Usage: python %prog [options] inputfiles. Use option -h for help information.")
	parser.add_option("-o", "--output", dest="output_file", default="../results_test_combined.txt", help='output file')
	parser.add_option("-c", "--classes", dest="classes", default='CN MCI AD', help='classes in the correct order (default: CN=0, MCI=1, AD=2, missing=3)')
	(options, args) = parser.parse_args()
	
	classes=re.split('_| ',options.classes)
	classes.append('missing')
	
	output_file=options.output_file
	
	output=StringIO.StringIO()
	table_header="\t".join(['ID','output','pCN','pMCI','pAD\n'])
	f=open(output_file,"w")
	output.write(table_header)
	
	for i,file in enumerate(args):
		path=os.path.dirname(file)
		filebase=os.path.basename(file)
		
		print file
		if not i:
			print 1
			ref_ids, ref_output,ref_prob_output, use_prob = process_results.read_result_file(path, filebase,classes)
		else:
			ids_original, output_original, prob_output_original, use_prob= process_results.read_result_file(path, filebase,classes)
			ref_ids, ref_output, ids, output1, prob_output = caddementia_statistics.match_reference(ref_ids, ref_output, ids_original, output_original, prob_output_original, 'test', '', '','all')
			combined=numpy.array(ref_prob_output)*numpy.array(prob_output)
			print combined
			ref_prob_output=combined.tolist()
	
	#Normalization
	x1=numpy.array(ref_prob_output)/numpy.array(ref_prob_output).sum(0)
	x1=x1.tolist()
	
	for i,id in enumerate(ref_ids):	
		table_template= string.Template('$id\t$output\t$cn\t$mci\t$ad')
		cn=x1[0][i]
		mci=x1[1][i]
		ad=x1[2][i]
		
		diag=''
		if cn>mci and cn>ad:
			diag=0
		elif mci>ad:
			diag=1
		else:
			diag=2
		
		print cn, mci, ad, diag
		new_line=table_template.safe_substitute(id=id, output='%d'%diag, cn='%.5f'%cn, mci='%.5f'%mci, ad='%.5f'%ad)
		print >>output, new_line 
		
	content=output.getvalue()
	print table_header
	print content
	f.write(content)
		
	f.close()
	output.close()
	
	
if __name__ == '__main__':
	main()				